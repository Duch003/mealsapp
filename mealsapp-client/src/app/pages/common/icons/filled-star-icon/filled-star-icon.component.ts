import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-filled-star-icon',
  templateUrl: './filled-star-icon.component.html',
  styleUrls: ['./filled-star-icon.component.css']
})
export class FilledStarIconComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
