import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BitbucketIconComponent } from './bitbucket-icon.component';

describe('BitbucketIconComponent', () => {
  let component: BitbucketIconComponent;
  let fixture: ComponentFixture<BitbucketIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BitbucketIconComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BitbucketIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
