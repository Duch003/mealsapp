import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-arrow-right-icon',
  templateUrl: './arrow-right-icon.component.html',
  styleUrls: ['./arrow-right-icon.component.css']
})
export class ArrowRightIconComponent implements OnInit {

  @Input() id: string;

  constructor() {
    this.id = '';
  }

  ngOnInit(): void {
  }

}
