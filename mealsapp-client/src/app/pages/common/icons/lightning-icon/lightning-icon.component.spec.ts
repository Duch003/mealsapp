import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LightningIconComponent } from './lightning-icon.component';

describe('LightningIconComponent', () => {
  let component: LightningIconComponent;
  let fixture: ComponentFixture<LightningIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LightningIconComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LightningIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
