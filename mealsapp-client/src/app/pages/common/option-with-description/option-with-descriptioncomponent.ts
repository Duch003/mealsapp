import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-meal-option',
  templateUrl: './option-with-description.component.html',
  styleUrls: ['./option-with-description.component.css']
})
export class OptionWithDescriptionComponent implements OnInit {

  @Input() title: string;
  @Input() url: string;
  @Input() description: string;
  @Input() tags: string;
  @Output() onClickEvent: EventEmitter<string>

  constructor() { 
    this.title = '';
    this.url = '';
    this.description = '';
    this.tags = '';
    this.onClickEvent = new EventEmitter<string>();
  }

  ngOnInit(): void {
  }

  onClickCallback(value: string) {
    this.onClickEvent.emit(value);
  }
}
