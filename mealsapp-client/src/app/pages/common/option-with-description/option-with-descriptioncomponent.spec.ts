import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OptionWithDescriptionComponent } from './option-with-descriptioncomponent';

describe('MealOptionComponent', () => {
  let component: OptionWithDescriptionComponent;
  let fixture: ComponentFixture<OptionWithDescriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OptionWithDescriptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OptionWithDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
