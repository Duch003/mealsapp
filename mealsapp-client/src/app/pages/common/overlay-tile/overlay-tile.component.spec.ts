import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayTileComponent } from './overlay-tile.component';

describe('OverlayTileComponent', () => {
  let component: OverlayTileComponent;
  let fixture: ComponentFixture<OverlayTileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OverlayTileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
