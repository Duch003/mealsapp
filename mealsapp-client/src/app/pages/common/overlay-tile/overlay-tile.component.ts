import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-overlay-tile',
  templateUrl: './overlay-tile.component.html',
  styleUrls: ['./overlay-tile.component.css']
})
export class OverlayTileComponent implements OnInit {

  @Input() title: string;
  @Input() url: string;
  @Output() onClickEvent: EventEmitter<string>;
  hoverDetected: boolean;

  constructor() { 
    this.title = '';
    this.url = '';
    this.hoverDetected = false;
    this.onClickEvent = new EventEmitter<string>();
  }

  ngOnInit(): void {
  }

  clickCallback(name: string) {
    this.onClickEvent.emit(name);
  }
}
