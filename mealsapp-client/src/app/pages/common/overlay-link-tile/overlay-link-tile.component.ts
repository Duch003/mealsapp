import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-overlay-link-tile',
  templateUrl: './overlay-link-tile.component.html',
  styleUrls: ['./overlay-link-tile.component.css']
})
export class OverlayLinkTileComponent implements OnInit {

  @Input() title: string;
  @Input() url: string;
  @Input() id: string;
  hoverDetected: boolean;

  constructor() { 
    this.title = '';
    this.url = '';
    this.id = '';
    this.hoverDetected = false;
  }

  ngOnInit(): void {
  }
}
