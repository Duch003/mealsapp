import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayLinkTileComponent } from './overlay-link-tile.component';

describe('OverlayLinkTileComponent', () => {
  let component: OverlayLinkTileComponent;
  let fixture: ComponentFixture<OverlayLinkTileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OverlayLinkTileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayLinkTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
