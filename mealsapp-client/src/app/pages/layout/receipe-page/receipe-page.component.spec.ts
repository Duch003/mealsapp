import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceipePageComponent } from './receipe-page.component';

describe('ReceipePageComponent', () => {
  let component: ReceipePageComponent;
  let fixture: ComponentFixture<ReceipePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReceipePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceipePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
