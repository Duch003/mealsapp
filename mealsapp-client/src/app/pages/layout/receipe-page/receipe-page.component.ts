import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import FullMeal from 'src/app/models/api/fullMeal';
import { DefaultValueService } from 'src/app/services/defaultValueService/default-value.service';
import { IngridientsMapperService } from 'src/app/services/ingridentsMapperService/ingridients-mapper.service';
import { LogService } from 'src/app/services/logService/log.service';
import { MealsService } from 'src/app/services/mealsService/meals.service';
import { RedirectService } from 'src/app/services/redirectService/redirect.service';

@Component({
  selector: 'app-receipe-page',
  templateUrl: './receipe-page.component.html',
  styleUrls: ['./receipe-page.component.css']
})
export class ReceipePageComponent implements OnInit {
  meal: FullMeal;
  ingridients: string[];
  instructions: string[];
  youtubeUrl: SafeUrl;
  categories: string;


  constructor(
    private activatedRoute: ActivatedRoute,
    private mealsService: MealsService,
    private defaultValueService: DefaultValueService,
    private ingridientsMapperService: IngridientsMapperService,
    private sanitizer: DomSanitizer,
    private redirectService: RedirectService,
    private logService: LogService) {
      this.meal = this.defaultValueService.getFullMeal();
      this.ingridients = [];
      this.instructions = [];
      this.youtubeUrl = '';
      this.categories = '';
    }

  ngOnInit(): void {
    const paramId = this.activatedRoute.snapshot.queryParamMap.get('id');
    if(!paramId 
      || paramId === null 
      || paramId.length < 1 
      || Number(paramId) === NaN 
      || Number(paramId) > Number.MAX_SAFE_INTEGER 
      || Number(paramId) < Number.MIN_SAFE_INTEGER ) {
        this.redirectService.notFound();
        return;
    }

    this.mealsService.searchMealsById(paramId).subscribe({
      next: response => {
        if(!response.meals || response.meals === null || response.meals.length < 1) {
          this.redirectService.notFound();
          return;
        }
        this.meal = response.meals[0];
        this.ingridients = this.ingridientsMapperService.prepareIngridients(this.meal);
        this.instructions = this.meal.strInstructions.split('\n');
        this.categories = `${this.meal.strArea}, ${this.meal.strCategory}`;
        if(this.meal.strTags !== null) {
          this.categories += `, ${this.meal.strTags.split(',').join(', ')}`;
        }
        const url = this.meal.strYoutube!
          .replace('/watch?v=', '/embed/')
          .replace('youtube.com', 'youtube-nocookie.com');
        this.youtubeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
      },
      error: error => {
        this.logService.logErrorOnApplicationInsight(error);
        this.redirectService.error();
      }
    });
  }
}
