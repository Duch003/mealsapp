import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CategoryItem from 'src/app/models/api/categoryItem';
import FilterMeal from 'src/app/models/api/filterMeal';
import FullCategory from 'src/app/models/api/fullCategory';
import { LogService } from 'src/app/services/logService/log.service';
import { MealsService } from 'src/app/services/mealsService/meals.service';
import { RedirectService } from 'src/app/services/redirectService/redirect.service';

@Component({
  selector: 'app-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.css']
})
export class CategoriesPageComponent implements OnInit {

  returnThumbnail: string;
  categories: FullCategory[];
  meals: FilterMeal[];

  constructor(
    private mealsService: MealsService,
    private logService: LogService,
    private redirectService: RedirectService,
    private activatedRoute: ActivatedRoute
  ) { 
    this.categories = [];
    this.meals = [];
    this.returnThumbnail = './../../../../assets/return.png';
  }

  ngOnInit(): void {
    const paramCategory = this.activatedRoute.snapshot.queryParamMap.get('category');
    if(paramCategory) {
      this.getMealsByCategories(paramCategory);
    }

    this.mealsService.getAllCategoriesWithDetails().subscribe(
      {
        next: response => {
          if(response.categories && response.categories !== null && response.categories.length > 0) {
            this.categories = response.categories;
          }
        },
        error: error => {
          this.logService.logErrorOnApplicationInsight(error);
          this.redirectService.error();
        }
      }
    )
  }

  getMealsByCategories(category: string) {
    this.mealsService.filterByCategory(category).subscribe({
      next: response => {
        if(response.meals && response.meals !== null) {
          this.meals = response.meals
        }
      },
      error: error => {
        this.logService.logErrorOnApplicationInsight(error);
        this.redirectService.error();
      }
    });
  }

  returnToCategories() {
    this.meals = [];
  }
}
