import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  @Output() mealNameChangedEvent: EventEmitter<string | null>;

  constructor() { 
    this.mealNameChangedEvent = new EventEmitter<string | null>();
  }

  searchByName(event: Event) {
    const inputEvent = event as InputEvent;
    const htmlElement = inputEvent.target as HTMLInputElement;
    this.mealNameChangedEvent.emit(htmlElement.value);
  }

  ngOnInit(): void {
  }

}
