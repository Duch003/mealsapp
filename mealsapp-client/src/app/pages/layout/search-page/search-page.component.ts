import { Component, OnInit } from '@angular/core';
import FullMeal from 'src/app/models/api/fullMeal';
import { MealsService } from 'src/app/services/mealsService/meals.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {

  meals: FullMeal[];

  constructor(private mealsService: MealsService) { 
    this.meals = [];
  }

  ngOnInit(): void {
  }

  searchByName(value: string | null) {
    if(!value || value === null || value.length < 1) {
      this.meals = [];
      return;
    }

    this.mealsService.searchMealsByName(value).subscribe({
      next: response => {
        if(response.meals) {
          this.meals = response.meals
        } else {
          this.meals = [];
        }
      },
      error: error => console.log(error)
    });
  }

}
