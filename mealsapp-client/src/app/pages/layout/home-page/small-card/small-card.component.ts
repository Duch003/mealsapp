import { Component, Input, OnInit } from '@angular/core';
import FullMeal from 'src/app/models/api/fullMeal';
import { DefaultValueService } from 'src/app/services/defaultValueService/default-value.service';

@Component({
  selector: 'app-small-card',
  templateUrl: './small-card.component.html',
  styleUrls: ['./small-card.component.css']
})
export class SmallCardComponent implements OnInit {

  @Input() meal: FullMeal;

  constructor(private defaultValueService: DefaultValueService) { 
    this.meal = this.defaultValueService.getFullMeal();
  }

  ngOnInit(): void {
  }

}
