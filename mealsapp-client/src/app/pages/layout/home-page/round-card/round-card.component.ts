import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import Category from 'src/app/models/api/fullCategory';
import { DefaultValueService } from 'src/app/services/defaultValueService/default-value.service';

@Component({
  selector: 'app-round-card',
  templateUrl: './round-card.component.html',
  styleUrls: ['./round-card.component.css']
})
export class RoundCardComponent implements OnInit, OnChanges {

  @Input() category: Category

  constructor(private defaultValueService: DefaultValueService) { 
    this.category = this.defaultValueService.getCategory();
  }

  ngOnChanges(changes: SimpleChanges): void {  
  }

  ngOnInit(): void {
  }

}
