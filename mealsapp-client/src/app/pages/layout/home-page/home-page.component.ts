import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Category from 'src/app/models/api/fullCategory';
import FullMeal from 'src/app/models/api/fullMeal';
import { DefaultValueService } from 'src/app/services/defaultValueService/default-value.service';
import { LogService } from 'src/app/services/logService/log.service';
import { MealsService } from 'src/app/services/mealsService/meals.service';
import { RedirectService } from 'src/app/services/redirectService/redirect.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  randomMeal: FullMeal;
  categories: Category[];
  popularMeals: FullMeal[];
  sweetMeals: FullMeal[];

  private popularMealsIds: string[] = ['52878', '52945', '52771'];
  private sweetMealsIds: string[] = ['52883', '52909', '52958'];

  constructor(
    private mealsService: MealsService,
    private defaultValueService: DefaultValueService,
    private router: Router,
    private logService: LogService,
    private redirectService: RedirectService) { 
    this.randomMeal = this.defaultValueService.getFullMeal();
    this.categories = [];
    this.popularMeals = [];
    this.sweetMeals = [];
  }

  ngOnInit(): void {
    this.getRandomMeal();
    this.getAllCategories();
    this.getPopularMeals();
    this.getSweetMeals();
  }

  private getRandomMeal() {
    this.mealsService.pickRandomMeal().subscribe(
      {
        next: mealsResponse => {
          this.randomMeal = mealsResponse.meals[0];
        },
        error: error => this.handleError(error)
      }
    );
  }
  
  private getAllCategories() {
    this.mealsService.getAllCategoriesWithDetails().subscribe({
      next: mealResponse => {
        this.categories = mealResponse.categories;
      },
      error: error => this.handleError(error)
    });
  }

  private getPopularMeals() {
    //It can fail only if any of the found meals is removed. Unfortunately, no better way to get three samples
    for(let id of this.popularMealsIds) {
      this.mealsService.searchMealsById(id).subscribe({
        next: response => {
          this.popularMeals.push(response.meals![0])
        },
        error: error => this.handleError(error)
      });
    }
  }

  private getSweetMeals() {
    //It can fail only if any of the found meals is removed. Unfortunately, no better way to get three samples
    for(let id of this.sweetMealsIds) {
      this.mealsService.searchMealsById(id).subscribe({
        next: response => {
          this.sweetMeals.push(response.meals![0])
        },
        error: error => this.handleError(error)
      });
    }
  }

  private handleError(error: any) {
    this.logService.logErrorOnApplicationInsight(error);
    this.redirectService.error();
  }
}
