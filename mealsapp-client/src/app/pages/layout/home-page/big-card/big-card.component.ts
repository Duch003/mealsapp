import { Component, Input, OnChanges, OnInit, SimpleChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked } from '@angular/core';
import FullMeal from 'src/app/models/api/fullMeal';
import { DefaultValueService } from 'src/app/services/defaultValueService/default-value.service';
import { IngridientsMapperService } from 'src/app/services/ingridentsMapperService/ingridients-mapper.service';

@Component({
  selector: 'app-big-card',
  templateUrl: './big-card.component.html',
  styleUrls: ['./big-card.component.css']
})
export class BigCardComponent implements OnInit, OnChanges {

  @Input() meal: FullMeal;
  listOfIngridients: string[];
  categories: string;
  title: string;
  thumbnail: string;
  id: string;

  constructor(private defaultValueService: DefaultValueService,
    private ingridientsMapperService: IngridientsMapperService) { 
    this.listOfIngridients = [];
    this.meal = this.defaultValueService.getFullMeal();
    this.categories = '';
    this.title = '';
    this.thumbnail = '';
    this.id = '';
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.listOfIngridients = this.ingridientsMapperService.prepareIngridients(this.meal);
    this.categories = `${this.meal.strArea}, ${this.meal.strCategory}`;
    if(this.meal.strTags !== null) {
      this.categories += `, ${this.meal.strTags.split(',').join(', ')}`;
    }
    this.title = this.meal.strMeal;
    this.thumbnail = this.meal.strMealThumb;
    this.id = this.meal.idMeal;
  }

  ngOnInit(): void { }
}
