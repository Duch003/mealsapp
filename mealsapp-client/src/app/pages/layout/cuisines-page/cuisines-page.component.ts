import { Component, OnInit } from '@angular/core';
import FilterMeal from 'src/app/models/api/filterMeal';
import Cuisine from 'src/app/models/custom/cuisine';
import { CountryFlagService } from 'src/app/services/countryFlagService/country-flag.service';
import { LogService } from 'src/app/services/logService/log.service';
import { MealsService } from 'src/app/services/mealsService/meals.service';
import { RedirectService } from 'src/app/services/redirectService/redirect.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-cuisines-page',
  templateUrl: './cuisines-page.component.html',
  styleUrls: ['./cuisines-page.component.css']
})
export class CuisinesPageComponent implements OnInit {

  returnThumbnail: string;
  cuisines: Cuisine[];
  meals: FilterMeal[];
  private unknownFlag: string;

  constructor(
    private mealsService: MealsService,
    private countryFlagService: CountryFlagService,
    private redirectService: RedirectService,
    private logServce: LogService
  ) { 
    this.cuisines = [];
    this.unknownFlag = './../../../assets/UnknownFlag.png';
    this.returnThumbnail = './../../../../assets/return.png';
    this.meals = [];
  }

  ngOnInit(): void {
    this.mealsService.getAllAreas().subscribe(
      {
        next: response => {
          response.meals.forEach((country, index) => {
            const flag = this.countryFlagService.getCountryCode(country.strArea);

            if(flag === null) {
              this.cuisines.push({
                area: country.strArea,
                flagUrl: this.unknownFlag
              });
            } else {
              this.cuisines.push({
                area: country.strArea,
                flagUrl: `${environment.flagApi}/${flag}`
              });
            }
          });
        },
        error: error => {
          this.logServce.logErrorOnApplicationInsight(error);
          this.redirectService.error();
        }
      }
    )
  }

  getMealsByCuisine(cuisine: string) {
    this.mealsService.filterByArea(cuisine).subscribe({
      next: response => {
        console.log(response);
        if(response.meals && response.meals !== null) {
          this.meals = response.meals
        }
      },
      error: error => {
        this.logServce.logErrorOnApplicationInsight(error);
        this.redirectService.error();
      }
    });
  }

  returnToCuisines() {
    this.meals = [];
  }
}
