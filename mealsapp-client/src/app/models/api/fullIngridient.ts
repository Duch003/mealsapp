export default interface FullIngridient {
    idIngredient: string,
    strIngredient: string,
    strDescription: string,
    strType: string | null
}