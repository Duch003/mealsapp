export default interface FilterMeal {
    strMeal: string,
    strMealThumb: string,
    idMeal: string
}