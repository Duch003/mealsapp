export default interface Cuisine {
    area: string,
    flagUrl: string
}