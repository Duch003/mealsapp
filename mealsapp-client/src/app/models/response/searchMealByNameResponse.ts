import FullMeal from "../api/fullMeal";

export default interface SearchMealByNameResponse {
    meals: FullMeal[] | null
}