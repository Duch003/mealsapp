import FilterMeal from "../api/filterMeal";

export default interface FilterByCategoryResponse {
    meals: FilterMeal[] | null
}