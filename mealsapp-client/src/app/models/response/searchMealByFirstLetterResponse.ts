import FullMeal from "../api/fullMeal";

export default interface SearchMealByFirstLetterResponse {
    meals: FullMeal[] | null
}