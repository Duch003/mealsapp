import CategoryItem from "../api/categoryItem";

export default interface ListAllCategoriesNamesResponse {
    meals: CategoryItem[]
}