import FullMeal from "../api/fullMeal";

export default interface SearchMealByIdResponse {
    meals: FullMeal[] | null
}