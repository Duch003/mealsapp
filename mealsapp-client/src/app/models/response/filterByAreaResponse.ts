import FilterMeal from "../api/filterMeal";

export default interface FilterByAreaResponse {
    meals: FilterMeal[] | null
}