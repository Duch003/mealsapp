import AreaItem from "../api/areaItem";

export default interface ListAllAreasNamesResponse {
    meals: AreaItem[]
}