import FullCategory from "../api/fullCategory";

export default interface ListAllCategoriesWithDetailsResponse {
    categories: FullCategory[]
}