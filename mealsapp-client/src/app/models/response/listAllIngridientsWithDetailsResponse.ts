import FullIngridient from "../api/fullIngridient";

export default interface ListAllIngridientsWithDetailsResponse {
    meals: FullIngridient[]
}