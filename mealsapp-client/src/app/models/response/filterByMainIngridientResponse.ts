import FilterMeal from "../api/filterMeal";

export default interface FilterByMainIngridientResponse {
    meals: FilterMeal[] | null
}