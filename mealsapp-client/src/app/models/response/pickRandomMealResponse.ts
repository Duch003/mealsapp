import FullMeal from "../api/fullMeal";

export default interface PickRandomMealResponse {
    meals: FullMeal[]
}