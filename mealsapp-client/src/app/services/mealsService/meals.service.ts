import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import FilterByAreaResponse from 'src/app/models/response/filterByAreaResponse';
import FilterByCategoryResponse from 'src/app/models/response/filterByCategoryResponse';
import FilterByMainIngridientResponse from 'src/app/models/response/filterByMainIngridientResponse';
import ListAllAreasNamesResponse from 'src/app/models/response/listAllAreasNamesResponse';
import ListAllCategoriesNamesResponse from 'src/app/models/response/listAllCategoriesNamesResponse';
import ListAllCategoriesWithDetailsResponse from 'src/app/models/response/listAllCategoriesWithDetailsResponse';
import ListAllIngridientsWithDetailsResponse from 'src/app/models/response/listAllIngridientsWithDetailsResponse';
import PickRandomMealResponse from 'src/app/models/response/pickRandomMealResponse';
import SearchMealByFirstLetterResponse from 'src/app/models/response/searchMealByFirstLetterResponse';
import SearchMealByIdResponse from 'src/app/models/response/searchMealByIdResponse';
import SearchMealByNameResponse from 'src/app/models/response/searchMealByNameResponse';
import { environment } from 'src/environments/environment';
import { HttpService } from '../httpService/http.service';

@Injectable({
  providedIn: 'root'
})
export class MealsService {

  constructor(private http: HttpService) { 
  }

  searchMealsByName(name: string): Observable<SearchMealByNameResponse> {
    return this.http.get<SearchMealByNameResponse>(this.searchByNameUrl(name));
  }

  searchMealsByFirstLetter(name: string): Observable<SearchMealByFirstLetterResponse> {
    return this.http.get<SearchMealByFirstLetterResponse>(this.listMealsByFirstLetterUrl(name));
  }

  searchMealsById(id: string): Observable<SearchMealByIdResponse> {
    return this.http.get<SearchMealByIdResponse>(this.lookupFullMealDetailsByIdUrl(id));
  }

  pickRandomMeal(): Observable<PickRandomMealResponse> {
    return this.http.get<PickRandomMealResponse>(this.lookupSingleRandomMealUrl());
  }

  getAllCategoriesWithDetails(): Observable<ListAllCategoriesWithDetailsResponse> {
    return this.http.get<ListAllCategoriesWithDetailsResponse>(this.listAllMealCategoriesUrl());
  }

  getAllCategories(): Observable<ListAllCategoriesNamesResponse> {
    return this.http.get<ListAllCategoriesNamesResponse>(this.listAllCategoriesUrl());
  }

  getAllAreas(): Observable<ListAllAreasNamesResponse> {
    return this.http.get<ListAllAreasNamesResponse>(this.listAllAreasUrl());
  }

  getAllIngridientsWithDetails(): Observable<ListAllIngridientsWithDetailsResponse> {
    return this.http.get<ListAllIngridientsWithDetailsResponse>(this.listAllIngridientsUrl());
  }

  filterByMainIngridient(name: string): Observable<FilterByMainIngridientResponse> {
    return this.http.get<FilterByMainIngridientResponse>(this.filterByMainIngridientUrl(name));
  }

  filterByCategory(name: string): Observable<FilterByCategoryResponse> {
    return this.http.get<FilterByCategoryResponse>(this.filterByCategoryUrl(name));
  }

  filterByArea(name: string): Observable<FilterByAreaResponse> {
    return this.http.get<FilterByAreaResponse>(this.filterByAreaUrl(name));
  }

  private searchByNameUrl(name: string): string {
    return `${environment.mealApi}/search.php?s=${name}`;
  }

  private listMealsByFirstLetterUrl(name: string): string {
    return `${environment.mealApi}/search.php?f=${name}`;
  }

  private lookupFullMealDetailsByIdUrl(id: string): string {
    return `${environment.mealApi}/lookup.php?i=${id}`;
  }

  private lookupSingleRandomMealUrl(): string {
    return `${environment.mealApi}/random.php`;
  }

  private listAllMealCategoriesUrl(): string {
    return `${environment.mealApi}/categories.php`;
  }

  private listAllCategoriesUrl(): string {
    return `${environment.mealApi}/list.php?c=list`;
  }

  private listAllAreasUrl(): string {
    return `${environment.mealApi}/list.php?a=list`;
  }

  private listAllIngridientsUrl(): string {
    return `${environment.mealApi}/list.php?i=list`;
  }

  private filterByMainIngridientUrl(name: string): string {
    return `${environment.mealApi}/filter.php?i=${name}`;
  }

  private filterByCategoryUrl(name: string): string {
    return `${environment.mealApi}/filter.php?c=${name}`;
  }

  private filterByAreaUrl(name: string): string {
    return `${environment.mealApi}/filter.php?a=${name}`;
  }
}
