import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor() { }

  getClientSideError(error: Error): string {
    return `Client side error: ${error}`;
  }

  getHttpError(error: Error): string {
    return `Http side error: ${error}`;
  }
}
