import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularPlugin } from '@microsoft/applicationinsights-angularplugin-js';
import { ApplicationInsights } from '@microsoft/applicationinsights-web';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApplicationInsightsService {

  appInsights: ApplicationInsights;

  constructor() {
    this.appInsights = new ApplicationInsights({
      config: {
        instrumentationKey: environment.appInsightsConfig.instrumentationKey,
        enableAutoRouteTracking: true,
      },
      
    });
    this.appInsights.loadAppInsights();
    this.appInsights.trackPageView();
  }

  logError(error: Error) {
    this.appInsights.trackException({exception: error});
  }
}
