import { TestBed } from '@angular/core/testing';

import { IngridientsMapperService } from './ingridients-mapper.service';

describe('IngridientsMapperService', () => {
  let service: IngridientsMapperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IngridientsMapperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
