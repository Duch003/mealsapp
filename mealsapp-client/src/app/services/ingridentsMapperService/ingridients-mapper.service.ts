import { Injectable } from '@angular/core';
import FullMeal from 'src/app/models/api/fullMeal';

@Injectable({
  providedIn: 'root'
})
export class IngridientsMapperService {

  constructor() { }

  prepareIngridients(meal: FullMeal): string[] {
    const entries = Object.entries(meal);
    const ingridientsCount = entries.filter(entry => entry[0].startsWith('strIngredient') && entry[1] && entry[1] !== '').length;
    const output = [];
    for(let i = 1; i <= ingridientsCount; i++) {
      const key = 'strIngredient' + i;
      const value = 'strMeasure' + i;
      const ingridient = entries.find((entry) => {
        return entry[0] === key;
      });
      const measure = entries.find((entry) => {
        return entry[0] === value;
      });

      //some ingridents have no specific measure, for example: 'butter': ' ' <--just space as a pair
      let ingridientString = `${ingridient?.[1]}`;
      if(measure?.[1] && measure?.[1].trim().length > 0) {
        console.log(measure?.[1], measure?.[1].length);
        ingridientString += `: ${measure?.[1]}`;
      }
      output.push(ingridientString);
    }

    return output;
  }
}

//weird receipes:
//Irish, Side - Mustard champ (duplicate ingr.)
//Irish, Beef - Alcoholic Vegetable Shepherd's Pie - no measurments for some ingridients