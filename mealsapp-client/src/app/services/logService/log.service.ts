import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApplicationInsightsService } from '../applicationInsigthsService/application-insights.service';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private appInsightsService: ApplicationInsightsService) { }

  logErrorOnApplicationInsight(error: Error) {
    return this.appInsightsService.logError(error);
  }

  logError(error: Error | HttpErrorResponse) {
    console.error(error);
  }
}
