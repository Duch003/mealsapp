import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GlobalErrorHandler } from './handlers/global-error-handler.service';
import { RouterModule } from '@angular/router';
import { HomePageComponent } from './pages/layout/home-page/home-page.component';
import { PageNotFoundComponent } from './pages/common/page-not-found/page-not-found.component';
import { ReceipePageComponent } from './pages/layout/receipe-page/receipe-page.component';

import { LayoutComponent } from './pages/layout/layout.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LightningIconComponent } from './pages/common/icons/lightning-icon/lightning-icon.component';
import { ArrowRightIconComponent } from './pages/common/icons/arrow-right-icon/arrow-right-icon.component';
import { FilledStarIconComponent } from './pages/common/icons/filled-star-icon/filled-star-icon.component';
import { BitbucketIconComponent } from './pages/common/icons/bitbucket-icon/bitbucket-icon.component';
import { LinkedinIconComponent } from './pages/common/icons/linkedin-icon/linkedin-icon.component';
import { SmallCardComponent } from './pages/layout/home-page/small-card/small-card.component';
import { BigCardComponent } from './pages/layout/home-page/big-card/big-card.component';
import { RoundCardComponent } from './pages/layout/home-page/round-card/round-card.component';
import { ErrorComponent } from './pages/common/error/error.component';
import { ShufflePipe } from './pipes/shufflePipe/shuffle.pipe';
import { SearchPageComponent } from './pages/layout/search-page/search-page.component';
import { SearchBarComponent } from './pages/layout/search-page/search-bar/search-bar.component';
import { CommonModule } from '@angular/common';
import { SplitStringPipe } from './pipes/splitStringPipe/split-string.pipe';
import { CategoriesPageComponent } from './pages/layout/categories-page/categories-page.component';
import { CuisinesPageComponent } from './pages/layout/cuisines-page/cuisines-page.component';
import { OverlayTileComponent } from './pages/common/overlay-tile/overlay-tile.component';
import { OptionWithDescriptionComponent } from './pages/common/option-with-description/option-with-descriptioncomponent';
import { OverlayLinkTileComponent } from './pages/common/overlay-link-tile/overlay-link-tile.component';
import { CreateTagsFromMealPipe } from './pipes/createTagsFromMealPipe/create-tags-from-meal.pipe';
import { SliceWithDotsPipe } from './pipes/sliceWithDotsPipe/slice-with-dots.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HomePageComponent,
    LightningIconComponent,
    ArrowRightIconComponent,
    FilledStarIconComponent,
    BitbucketIconComponent,
    LinkedinIconComponent,
    SmallCardComponent,
    BigCardComponent,
    RoundCardComponent,
    ErrorComponent,
    ShufflePipe,
    SearchPageComponent,
    SearchBarComponent,
    ReceipePageComponent,
    SplitStringPipe,
    CategoriesPageComponent,
    CuisinesPageComponent,
    OverlayTileComponent,
    OptionWithDescriptionComponent,
    OverlayLinkTileComponent,
    CreateTagsFromMealPipe,
    SliceWithDotsPipe,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'home', component: HomePageComponent },
      { path: 'search', component: SearchPageComponent },
      { path: 'receipe', component: ReceipePageComponent },
      { path: 'categories', component: CategoriesPageComponent },
      { path: 'cuisines', component: CuisinesPageComponent },
      { path: 'error', component: ErrorComponent },
      { path: 'notFound', component: PageNotFoundComponent },
      { path: '', redirectTo: '/home', pathMatch: 'full' },
      { path: '**', redirectTo: '/notFound', pathMatch: 'full' },
    ]),
    NgbModule
  ],
  providers: [
    { provide: ErrorHandler, useClass: GlobalErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
