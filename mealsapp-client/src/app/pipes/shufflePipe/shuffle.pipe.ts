import { Pipe, PipeTransform } from '@angular/core';
import Category from 'src/app/models/api/fullCategory';

@Pipe({
  name: 'shuffle'
})
export class ShufflePipe implements PipeTransform {

  transform(array: any[]): any[] {
    if(array.length < 2) {
      return array;
    }

    let currentIndex = array.length;
    while(currentIndex != 0) {
      const randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
      [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
    }
    
    return array;
  }

}
