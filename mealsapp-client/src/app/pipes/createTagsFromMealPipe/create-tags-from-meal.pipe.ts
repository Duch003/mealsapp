import { Pipe, PipeTransform } from '@angular/core';
import FullMeal from 'src/app/models/api/fullMeal';

@Pipe({
  name: 'createTagsFromMeal'
})
export class CreateTagsFromMealPipe implements PipeTransform {

  transform(meal: FullMeal): string {
    let tags = [];

    if(meal.strArea) {
      tags.push(meal.strArea);
    }

    if(meal.strCategory) {
      tags.push(meal.strCategory);
    }

    if(meal.strTags) {
      tags = tags.concat(meal.strTags.split(','));
    }

    return tags.join(', ');
  }
}
