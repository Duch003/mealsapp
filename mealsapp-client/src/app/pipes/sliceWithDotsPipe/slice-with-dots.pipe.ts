import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sliceWithDots'
})
export class SliceWithDotsPipe implements PipeTransform {

  transform(value: string, length: number): string {
    if(value.length <= length) {
      return value;
    }

    const dots = '(...)';
    if(value.length <= dots.length) {
      return value;
    }

    return value.slice(0, length - dots.length).trim() + dots;
  }

}
