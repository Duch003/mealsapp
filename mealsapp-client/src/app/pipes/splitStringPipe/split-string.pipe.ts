import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'splitString'
})
export class SplitStringPipe implements PipeTransform {

  transform(value: string, separator: string, chunk: number): string {
    return value.split(separator)[chunk];
  }

}
