import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';
import { ErrorService } from '../services/errorService/error.service';
import { LogService } from '../services/logService/log.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalErrorHandler implements ErrorHandler {

  constructor(
    private logService: LogService,
    private errorService: ErrorService
  ) { }

  handleError(error: Error | HttpErrorResponse): void {
    if(error instanceof HttpErrorResponse) {
      alert(this.errorService.getHttpError(error));
    } else {
      this.logService.logErrorOnApplicationInsight(error);
      alert(this.errorService.getClientSideError(error));
    }

    this.logService.logError(error);
    this.logService.logErrorOnApplicationInsight(error);
  }
}
